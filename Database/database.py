import os.path

import json
from guiElements.question_overview import Quiz

class database():

    def __init__(self, path = "JsonDatabase"):
        self.path = path

        # Is running on pi?
        piPath = "/home/pi/BuzzedClassroom/"
        if os.path.isdir(os.path.join(piPath)):
            self.path = os.path.join(piPath, self.path)
            print(self.path)

        # Load data
        with open(os.path.join(self.path, "Quiz")) as json_data:
            self.d = json.load(json_data)
        with open(os.path.join(self.path, "Questions")) as json_data:
            self.q = json.load(json_data)

    def getQuizNames(self):
        return list(self.d["QuizDict"].keys())

    def getQuiz(self, name):
        quiz = self.d["QuizDict"][name]
        questions = []
        for i in quiz:
            qu = Quiz()
            qu.contentFromJson(self.q["questionDir"][str(i)])
            questions.append(qu)
        return questions
